# -*- coding: utf-8 -*-
__author__ = 'wojtek'
import os
import sys
import getopt


def install(filename):
    f = open(filename)
    reqs = f.read().split("\n")

    for req in reqs:
        # ignore comments or pip options
        if req.startswith('--') or req.startswith('#'):
            continue
        # ignore blank lines
        if len(req) > 0:
            os.system("easy_install %s" % req)


def usage():
    print 'WRONG USAGE, please -> python install_all.py filename.txt'
	
def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hr:")
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        print opt, '->', arg
        if opt in ('-h'):
            usage()
            sys.exit(2)
        if opt in ('-r'):
            install(arg)
			
if __name__ == "__main__":
    main(sys.argv[1:])