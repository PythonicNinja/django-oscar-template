django-oscar-template
==============================

django-oscar-template shop

Developer Installation
-----------------------

For getting this running on your local machine:

1. Set up a virtualenv and activate it.

	virtualenv --no-site-packages env
	
	WINDOWS
		env\Scripts\activate
	LINUX
		env\bin\activate

2. Install all the supporting libraries into your virtualenv::
	
    python install_all.py -r requirements.txt

	(it is an wraper for easy_install specialy for windows where c dependened libs are hard to install)

3.	Start Django project with command::

	python env\Scripts\django-admin.py startproject --template=https://bitbucket.org/vojtek/django-twoscoops-oscar-template/get/master.zip --extension=py,rst,rb,html YourShopname
	
4. Go to settings/ and modify local.py (this file is specified in gitignore and manage.py files)

5. Run commands::

    python manage.py syncdb
    python manage.py migrate
    python manage.py runserver